
Tema proiectului va surprinde atractiile turistice din cele mai cunoscute tari ale Europei, acest lucru
fiind dezvoltat cu ajutorul api-ului webcams travel. In principal vom crea o aplicatie de tip single-page
cu elemente de tip drop-down pentru selectarea tarii si a orasului, iar in functie de alegerile facute
vor aparea alte elemente prin care se vor afisa atractiile turistice din respectivele orase (si webcam-urile
unde este cazul). Fiecare atractie in parte va corespunde cu o portiune de text care va descrie un scurt istoric
al monumentului, muzeului, cladirii, etc. Mai mult, tot cu ajutorul api-ului webcams travel, 
utilizatorul va avea posibilitatea vizualizarii atractiilor turistice in functie de locatia lui, 
pe o raza de cativa kilometri.
